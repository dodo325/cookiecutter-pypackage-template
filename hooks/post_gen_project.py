from __future__ import print_function

import os
import random
import shutil
import string

try:
    # Inspired by
    # https://github.com/django/django/blob/master/django/utils/crypto.py
    random = random.SystemRandom()
    using_sysrandom = True
except NotImplementedError:
    using_sysrandom = False


TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m [WARNING]: "
INFO = "\x1b[1;33m [INFO]: "
HINT = "\x1b[3;33m"
SUCCESS = "\x1b[1;32m [SUCCESS]: "


def remove_docker_files():
    file_names = ["Dockerfile", ".dockerignore"]
    for file_name in file_names:
        os.remove(file_name)

def remove_open_source_files():
    file_names = ["CONTRIBUTORS.txt", "LICENSE"]
    for file_name in file_names:
        os.remove(file_name)


def remove_gplv3_files():
    file_names = ["COPYING"]
    for file_name in file_names:
        os.remove(file_name)


def remove_cli_files():
    file_names = ["__main__.py", "cli.py"]
    for file_name in file_names:
        os.remove(os.path.join("{{cookiecutter.project_slug}}", file_name))


def remove_fastapi_files():
    file_names = ["api.py"]
    for file_name in file_names:
        os.remove(os.path.join("{{cookiecutter.project_slug}}", file_name))


def remove_streamlit_files():
    file_names = ["streamlit.py"]
    for file_name in file_names:
        os.remove(os.path.join("{{cookiecutter.project_slug}}", file_name))


def main():
    if "{{ cookiecutter.open_source_license }}" == "Not open source":
        remove_open_source_files()
    if "{{ cookiecutter.open_source_license}}" != "GPLv3":
        remove_gplv3_files()
    if "{{ cookiecutter.use_typer_cli}}" != "y":
        remove_cli_files()
    if "{{ cookiecutter.use_fastapi}}" != "y":
        remove_fastapi_files()
    if "{{ cookiecutter.use_streamlit}}" != "y":
        remove_streamlit_files()
    if "{{ cookiecutter.use_docker}}" != "y":
        remove_docker_files()
    print(SUCCESS + "Project initialized, keep up the good work!" + TERMINATOR)


if __name__ == "__main__":
    main()
