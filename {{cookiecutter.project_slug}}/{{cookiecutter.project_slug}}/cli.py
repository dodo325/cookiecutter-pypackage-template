import typer
import uvicorn

from .api import app as api_app

app = typer.Typer()


@app.command()
def hello(name: str):
    typer.echo(f"Hello {name}")

{% if cookiecutter.use_fastapi == 'y' %}
@app.command()
def api(port: int = 8000):
    uvicorn.run(api_app, host="0.0.0.0", port=port)

{% endif %}
if __name__ == "__main__":
    app()
