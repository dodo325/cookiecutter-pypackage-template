"""Package for {{cookiecutter.project_name}}"""

__title__ = "{{cookiecutter.project_slug}}"
__summary__ = "{{cookiecutter.description}}"
__url__ = "{{cookiecutter.url}}"

__version__ = "{{cookiecutter.version}}"

__author__ = "{{cookiecutter.author_name}}"
__email__ = "{{cookiecutter.email}}"

__license__ = "{{cookiecutter.open_source_license}}"

__all__ = [
    "__title__",
    "__summary__",
    "__uri__",
    "__version__",
    "__author__",
    "__email__",
    "__license__",
]
