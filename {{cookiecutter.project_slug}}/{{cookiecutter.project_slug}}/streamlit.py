""" streamlit application """

import sys

import pandas as pd
import streamlit as st
from streamlit import cli as stcli


def main():
    st.write("Here's our first attempt at using data to create a table:")
    st.write(
        pd.DataFrame({"first column": [1, 2, 3, 4], "second column": [10, 20, 30, 40]})
    )


if __name__ == "__main__":
    if st._is_running_with_streamlit:
        main()
    else:
        sys.argv = ["streamlit", "run", sys.argv[0]]
        sys.exit(stcli.main())
