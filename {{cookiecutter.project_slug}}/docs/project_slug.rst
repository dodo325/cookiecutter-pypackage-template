project\_slug package
=====================

Submodules
----------

project\_slug.api module
------------------------

.. automodule:: {{cookiecutter.project_slug}}.api
   :members:
   :undoc-members:
   :show-inheritance:

project\_slug.cli module
------------------------

.. automodule:: {{cookiecutter.project_slug}}.cli
   :members:
   :undoc-members:
   :show-inheritance:

project\_slug.streamlit module
------------------------------

.. automodule:: {{cookiecutter.project_slug}}.streamlit
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: {{cookiecutter.project_slug}}
   :members:
   :undoc-members:
   :show-inheritance:
